package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    @Override
    public int findNumber(RandomNumber number) {
        int lowerbound = number.getLowerbound();
        int upperbound = number.getUpperbound();

        return makeGuess(number, lowerbound, upperbound);
    }

    public int makeGuess(RandomNumber number, int lowerbound, int upperbound) {

        int mid = (upperbound + lowerbound) / 2;
        int guessResult = number.guess(mid);

        if (guessResult == 0 || lowerbound == upperbound)
            return mid;
        if (guessResult == 1)
            return makeGuess(number, lowerbound, mid);
        if (guessResult == -1)
            return makeGuess(number, mid + 1, upperbound); // i add 1 since whole number division rounds down and can lead to screnarios where the algorithm gets stuck
        
        throw new IllegalArgumentException("Something went wrong");
    }

}
